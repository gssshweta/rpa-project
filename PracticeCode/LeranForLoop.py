# coding=utf-8
# Loop over a list while keeping track of indexes with enumerate:
# for header, rows in zip(headers, columns):
#     print("{}: {}".format(header, ", ".join(rows)))
num =[1,2,3,4,5]
lines=["a","b","c","d","e","f"]
for num, line in enumerate(lines):
    print(num, line)

colors = ["red", "green", "blue", "purple"]
ratios = [0.2, 0.3, 0.1, 0.4]
for color, ratio in zip(colors, ratios):
    print("{}% {}".format(ratio * 100, color))

# n = int(input("Enter the number of rows you want to print?"))
# i, j = 0, 0
# for i in range(0, n):
#     print()
#     for j in range(0, i + 1):
#         print("*",end="")
#
# for i in range(4):
#     for j in range(4):
#         print("*",end="")


colors = ["red", "green", "blue", "purple"]
ratios = [0.2, 0.3, 0.1, 0.4]
for color, ratio in zip(colors, ratios):
    print(color,ratio)

for i in zip(colors, ratios):
    print(i)


for i,color in enumerate(colors):
    print (i,color)
