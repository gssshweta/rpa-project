from selenium import webdriver

import unittest

from Pages1.LoginPage import LoginPage


class LoginTest(unittest.TestCase):
    @classmethod
    def launchBrowser(self,browserType):

        if browserType=="chrome":
             self.driver = webdriver.Chrome(executable_path="C:\Users\sgoudar\PycharmProjects\SalesforceDemo\Drivers\chromedriver.exe")
        elif browserType=="ie":
            self.driver = webdriver.ie(executable_path="C:\Users\sgoudar\PycharmProjects\SalesforceDemo\Drivers\chromedriver.exe")
        elif browserType == "firefox":
            self.driver = webdriver.firefox(executable_path="C:\Users\sgoudar\PycharmProjects\SalesforceDemo\Drivers\chromedriver.exe")

        self.driver.maximize_window()
        self.driver.implicitly_wait(20)
        self.driver.set_page_load_timeout(40)
        self.driver.get("https://login.salesforce.com/?locale=au")

    def loginToApplicationURL(self,Url):
        self.driver.get("https://login.salesforce.com/?locale=au")

    def test_login(self):
        driver = self.driver
        loginPage=LoginPage(driver)
        loginPage.enterUserName("gsshweta44@gmail.com")
        loginPage.login_password_edit("Parva@1234")
        loginPage.clickLoginButton()

    @classmethod
    def test_tearDown(self):
         self.driver.close()
         self.driver.quit()

if __name__=='__main__':
	unittest.main()


