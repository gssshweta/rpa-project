class LoginPage():

    def __init__(self,driver):
        driver=self.driver
        self.login_user_edit     =self.driver.find_element_by_id("username")
        self.login_password_edit = self.driver.find_element_by_id("password")
        self.login_login_button  = self.driver.find_element_by_id("Login")

    def enterUserName(self,userName):
        self.driver.find_element_by_id(self.login_user_edit).sendkeys(userName)

    def enterPassword(self,passowrd):
        self.login_password_edit.sendkeys(passowrd)

    def clickLoginButton(self):
        self.login_password_edit.click()

